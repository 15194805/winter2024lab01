import java.util.Scanner;
public class TheHangman_Complex{
	public static void main(String[] args){

		//notes:
		// I really tried to stick with the original parameters of the functions, but I couldn't think of another way to make a "global" variable (the list)
		// that could be used by every function apart from making it a parameter of almost all the functions.
		// I have no idea if it actually works correctly.

		//intro
		System.out.println("Let's play Hangman! One player will choose a word. The second player will have to guess that word.");

		//settings
		System.out.print("\nPlayer 2, please look away from the screen.Player 1, please configure the settings:\n");

		Scanner sc = new Scanner(System.in);

		System.out.print("Word to be guessed:");
		String target = sc.nextLine();

		System.out.print("\nNumber of Guesses Player 2 can make:");
		int NumberOfChances= sc.nextInt();
		sc.nextLine();

		// pretty much the core array of the code. If this is messed with, everything go kaboom
		boolean[] ListOfLettersGuessed = new boolean[target.length()];

		//start of game
		System.out.print(String.format("\033[2J"));
		System.out.println("Player 2, you can start guessing by inputing one letter per guess. You have "+ NumberOfChances +" guesses left.");
		runGame(target,ListOfLettersGuessed, NumberOfChances);
	}
	//functions
	public static boolean isLetterinWord(String word, char c, boolean[] ListOfLettersGuessed){
		//returns postition of  c in word. if c is not  in word, return -1.
		boolean LetterIsInWord=false;
		for (int i=0;i<word.length();i++){
			if (toUpperCase(word.charAt(i))==toUpperCase(c)){
				ListOfLettersGuessed[i]=true;
				LetterIsInWord=true;
			}
		}
		return LetterIsInWord;
	}
	// returns char as uppercase
	public static char toUpperCase(char c){
		return (Character.toString(c).toUpperCase()).charAt(0);
	}
	//prints the letters that have been guessed correctly. If letter hasnt been guessed, substitute with an underline.
	public static void printWork(String word,boolean[] ListOfLettersGuessed){
		String output="";
		for (int i=0;i<word.length();i++){
			if (ListOfLettersGuessed[i]==true) output=output+" "+word.charAt(i);
			else output=output+" _";
		}

		System.out.println("Your result is ["+output+"]");


	}
	public static void runGame(String word, boolean[] ListOfLettersGuessed, int guessChances){

		Scanner scanner = new Scanner(System.in);

		while(guessChances>=0){

			//if all letters gussed, break loop and declare winner
			int hasWon=0;
			for (int i=0;i<word.length();i++){
				if (!ListOfLettersGuessed[i]){
					hasWon++;
				}
			}
			if (hasWon==0){
				System.out.println("Player 2 has won! the word was "+word+". They had "+guessChances+" chances left.");
				break;
			}
			// if guesses have run out, break loop and declare winner
			if (guessChances==0) {

				System.out.println("Player 1 has won! the word was "+word+".");
				break;
				}
			System.out.print("\nYour next guess:");

			//player guess
			char Player_guess= toUpperCase(scanner.next().charAt(0));

			if (!isLetterinWord(word, Player_guess, ListOfLettersGuessed)  ){
				//player  guessed wrongly!
				guessChances--;
				System.out.print("\nYou guessed wrongly.");
				printWork(word, ListOfLettersGuessed);
				System.out.println("You have "+guessChances+" chances left.");

			}
			else{
				//player guessed right!
				System.out.print("\nYou guessed correctly.");

				printWork(word,ListOfLettersGuessed);
			}
		}
	}
}